import * as description from './stacked_column.md';
import examples from './examples';

export default {
  description,
  examples,
};
