import SearchBoxByTypeDefaultExample from './search_box_by_type.default.example.vue';

export default [
  {
    name: 'Search Box By Type',
    items: [
      {
        id: 'search-box-by-type',
        name: 'default',
        component: SearchBoxByTypeDefaultExample,
      },
    ],
  },
];
